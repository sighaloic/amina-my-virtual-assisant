import sys

import speech_recognition as sr
import pyttsx3
import pywhatkit
import datetime
import wikipedia
import pyjokes

listener = sr.Recognizer()
engine = pyttsx3.init()
engine.setProperty('rate', 150)  # voice speed
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[1].id)


def talk(text):
    engine.say(text)
    engine.runAndWait()

def take_command():
    try:
        with sr.Microphone() as source:
            talk('How can I help you ?')
            print('listening...')
            voice = listener.listen(source)
            command = listener.recognize_google(voice)  # language="fr-FR"
            command = command.lower()
            if 'amina' in command:
                command = command.replace('amina', '')
                # talk(command)
    except:
        pass
    return command

def run_amina():
    global state
    state = 1

    command = take_command()
    print(command)
    if 'play' in command:  # check if it's a playing command
        song = command.replace('play', '')
        talk('playing' + song)
        pywhatkit.playonyt(song)
        talk('Bye, see you soon')
        state = 0
    elif 'time' in command:  # check if it's a time command
        time = datetime.datetime.now().strftime('%I:%M %p')
        print(time)
        talk('Current time is ' + time)
    elif 'who is' in command:  # check if it's a "who is"(information about a pers) command
        person = command.replace('who is', '')
        info = wikipedia.summary(person, 1)  # Whe just want one line
        print(info)
        talk(info)
    elif 'date' in command:  # check if it's a "date" (proposition to meet her at a dating) command
        talk('sorry I han a headache')
    elif 'are you single' in command:  # check if it's a "are you single"
        # (because she is in relationship with wifi) command
        talk('I am in a relationship with wifi')
    elif 'joke' in command:  # check if it's a "joke" (if you want her telling you a joke) command
        joke = pyjokes.get_joke()
        print(joke)
        # talk(pyjokes.get_joke())
        talk(joke)
    elif 'see you later' in command:  # check if it's a "bye bye" command
        talk('Bye, see you soon')
        state = 0
    else:
        talk('Please say the command again.')


while True:
    try:
        run_amina()
        print(state)
        if state == 0:
            break
    except Exception as e:
        talk('I am tired, please me, I need to rest')
        print(str(e))
        sys.exit()


# pip install pyttsx3 pypiwin32
# import pyttsx3
# One time initialization
# engine = pyttsx3.init()
# Set properties _before_ you add things to say
# engine.setProperty('rate', 150)   
# Speed percent (can go over 100)
# engine.setProperty('volume', 0.9) 
# Volume 0-1# Queue up things to say.
# There will be a short break between each one
# when spoken, like a pause between sentences.
# engine.say("You've got mail!")
# engine.say("You can queue up multiple items")
# Flush the say() queue and play the audio
# engine.runAndWait()
# Program will not continue execution until
# all speech is done talking
